$(document).ready(function () {
    let id = $('.like').data('id');
    let url = window.location.toString();
    let user;
    if (document.cookie.match(/likeButtonUsrId=.{16}/) === null) {
        user = randomId();
        document.cookie = 'likeButtonUsrId=' + user;
    } else {
        user = document.cookie.match(/likeButtonUsrId=.{16}/);
        user = user[0].split('=');
        user = user[1];
    }
    $.ajax({
        url: 'http://likeSystem.local/getLikesScript.php',
        type: 'POST',
        data: {id:id, url:url},
        dataType: 'json',
        success: function (result) {
            if (!result.error) {
                $('.counter').html(result.count);
            } else {
                alert(result.message);
                $('.counter').html(result.count);
            }
        }
    });

    $('.like').bind("click", function () {
        $.ajax({
            url: 'http://likeSystem.local/likeScript.php',
            type: 'POST',
            data: {id:id, url:url, user:user},
            dataType: 'json',
            success: function (result) {
                if(!result.error) {
                    $('.counter').html(result.count);
                } else {
                    alert(result.message);
                    $('.counter').html(result.count);
                }
            }
        });
    });
});

function randomId() {
    let chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz".split("");

    let str = "";
    for (let i = 0; i < 16; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }

    return str;
}