<!DOCTYPE html>
<html>
    <head>
        <script src="script/jquery.js" type="text/javascript"></script>
        <title>Тест Системы Лайков</title>
        <meta charset="utf-8">
        <script src="LikeButton/like.js" type="text/javascript"></script>
        <link href="LikeButton/like.css" rel="stylesheet">

    </head>
    <body>
        <div class="likeSystem">
            <button class="like" data-id="1">мне нравиться</button><span class="counter"></span>
        </div>
        <a href="anotherPage.php">Перейти на другую страницу</a>
    </body>
</html>
